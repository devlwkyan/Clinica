from management import *
from registering import *


class Main:
    
    def menu(self):
        option = int(input("""
        O que deseja fazer, digite o humero de acordo com a opção desejada:
        
        [0] - Cadastrar Paciente
        [1] - Cadastrar Médico
        [2] - Ver lista de paciêntes
        [3] - Ver lista de Médicos
        [4] - Marcar Consulta
        [5] - Ver listar de consultas
        [6] - Descadastrar Médico
        [7] - Desmarcar consulta
        [8] - Sair
        
        :"""))
        if option == 0:
            registering(0)
        elif option == 1:
            registering(1)
        elif option == 2:
            Management.listing(2)
        elif option == 3:
            Management.listing(3)
        elif option == 4:
            Management.appointment()
        elif option == 5:
            Management.listing(5)
        elif option == 6:
            Management.remover(6)
        elif option == 7:
            Management.remover(7)
        elif option == 8:
            pass
        else:
            menu()

Main().menu()
