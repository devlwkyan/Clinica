import main
import management as mngt
import json


def registering(n):
        if n == 0:
            mngt.Management.patient["Nome"] = str(input('Informe o nome do paciente: ')).title()
            mngt.Management.patient["Email"] = str(input('Informe o email do paciente: ')).lower()
            mngt.Management.patient["Telefone"] = int(input('Informe telefone do paciente: '))
            mngt.Management.patient["RG"] = float(input('Informe o RG do paciente: '))
            mngt.Management.patient["CPF"] = str(input('Informe CPF do paciente: '))
            mngt.Management.patient["Endereco"] = str(input('Informe o endereço do paciente: '))
            mngt.Management.patient["numCasa"] = str(input('Informe o numéro da casa do paciente: '))
            mngt.Management.patient["Bairro"] = str(input('Informe o bairro do paciente: '))
            mngt.Management.patient["Cidade"] = str(input('Informe a cidade do paciente: '))
            mngt.Management.patient["Estado"] = str(input('Informe o estado do paciente: '))
            mngt.Management.patient["planoSaude"] = str(input('Informe o plano de saúde do paciente: '))
            try:
                with open("patients.json") as pats:
                    p_patients = json.load(pats)
                with open("patiens.json", mode='w') as f:
                    pat_list = []
                    for k, v in mngt.Management.patient.items():
                        pat_list.append(k + ": " + v)
                        a = json.dumps(pat_list)
                        f.write(a)
            except:
                with open("patients.json", mode='w') as pats:
                    for k, v in mngt.Management.patient.items():
                        temp = json.dumps(k + v)
                        pats.write(temp)

            back_to_menu = int(input('''
                Cadastramento concluído, o que deseja fazer agora:
                [0] - Novo Cadastro de Paciente
                [1] - Voltar ao menu principal
                [2] - Sair
                :'''))

            if back_to_menu == 0:
                registering(0)
            elif back_to_menu == 1:
                main.Main()
            elif back_to_menu == 2:
                pass
            else:
                while True:
                    back_to_menu = int(input('''
                        Cadastramento concluído, o que deseja fazer agora:
                        [0] - Novo Cadastro de Paciente
                        [1] - Voltar ao menu principal
                        :'''))
                    if back_to_menu == 0:
                        registering(0)
                        break
                    elif back_to_menu == 1:
                        main.Main()
                        break
        if n == 1:
            mngt.Management.doctor["Nome"] = str(input('Informe o nome do paciente: ')).title()
            mngt.Management.doctor["Email"] = str(input('Informe o email do paciente: ')).lower()
            mngt.Management.doctor["Telefone"] = int(input('Informe telefone do paciente: '))
            mngt.Management.doctor["RG"] = float(input('Informe o RG do paciente: '))
            mngt.Management.doctor["CPF"] = str(input('Informe CPF do paciente: '))
            mngt.Management.doctor["Endereco"] = str(input('Informe o endereço do paciente: '))
            mngt.Management.doctor["numCasa"] = str(input('Informe o numéro da casa do paciente: '))
            mngt.Management.doctor["Bairro"] = str(input('Informe o bairro do paciente: ')).title()
            mngt.Management.doctor["Cidade"] = str(input('Informe a cidade do paciente: ')).title()
            mngt.Management.doctor["Estado"] = str(input('Informe o estado do paciente: ')).title()
            mngt.Management.doctor["planoSaude"] = str(input('Informe o plano de saúde do paciente: '))
            mngt.Management.doctor["CRM"] = int(input('Informe o CRM do médico: '))
            mngt.Management.doctor["Especialidade"] = str(input('Informe a especialidade do médico: '))
            try:
                with open("doctors.json") as pats:
                    p_patients = json.load(pats)
                with open("doctors.json", mode='w') as f:
                    for k, v in mngt.Management.patient.items():
                        temp = json.dumps(k + v)
                        f.write(temp)
            except:
                with open("doctors.json", mode='w') as pats:
                    for k, v in mngt.Management.patient.items():
                        temp = json.dumps(k + v)
                        pats.write(temp)

            back_to_menu = int(input('''
            Cadastramento concluído, o que deseja fazer agora:
            [0] - Novo Cadastro de Médico
            [1] - Voltar ao menu principal
            [2] - Sair
            :'''))

            if back_to_menu == 0:
                registering(1)
            elif back_to_menu == 1:
                main.Main()
            elif back_to_menu == 2:
                pass
            else:
                while True:
                    back_to_menu = int(input('''
                    Cadastramento concluído, o que deseja fazer agora:
                    [0] - Novo Cadastro de Paciente
                    [1] - Voltar ao menu principal
                    [2] - Sair
                    :'''))
                    if back_to_menu == 0:
                        registering(1)
                        break
                    elif back_to_menu == 1:
                        main.Main()
                        break
                    elif back_to_menu == 2:
                        break
